package com.example.shifuminco;

import com.marlou.shifuminco.AI;
import com.marlou.shifuminco.Difficulties;
import com.marlou.shifuminco.Fight;
import com.marlou.shifuminco.Game;
import com.marlou.shifuminco.Types;

import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void test_whoWins() {
        assertEquals(2, Fight.WhoWins(Types.Scissors, Types.Spock));
        assertEquals(2, Fight.WhoWins(Types.Scissors, Types.Stone));
        assertEquals(0, Fight.WhoWins(Types.Lizard, Types.Lizard));
        assertEquals(1, Fight.WhoWins(Types.Stone, Types.Scissors));
        assertEquals(1, Fight.WhoWins(Types.Stone, Types.Lizard));
    }

    @Test
    public void test_getSuperiorityTable() {
        assertEquals(2, Fight.getSuperiorityTable().size());
        for (int i = 0; i < Fight.getSuperiorityTable().size(); i++) {
            assertEquals(5,Fight.getSuperiorityTable().get(i).size());
        }
    }

    @Test
    public void test_game(){
        Game ag=new Game("Normal");
        for(int i=0;i<Game.getMaxRounds();i++){
            if(Types.Scissors==ag.Play(Types.Scissors))
                i-=1;
        }
        assertTrue(ag.numberOfLooses()>Game.getMaxRounds()/2^ag.numberOfWins()>Game.getMaxRounds()/2);
        assertTrue(ag.getIsGameFinished());
    }

    @Test
    public void test_Divide(){
        assertEquals(2,Game.getMaxRounds()/2);
    }
}