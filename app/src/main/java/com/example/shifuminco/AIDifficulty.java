package com.example.shifuminco;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.marlou.shifuminco.Difficulties;
import com.marlou.shifuminco.Game;

public class AIDifficulty extends AppCompatActivity {

    RadioGroup diffs;
    String radiotext;
    Button goGame;
    boolean checked = false;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a_i_difficulty);
        //
        goGame = findViewById(R.id.btnlaunch);
        diffs = findViewById(R.id.radiodiffs);

        diffs.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton diffSelect = findViewById(checkedId);
            radiotext = diffSelect.getText().toString();
            checked = true;
        });

        goGame.setOnClickListener((v) -> {
            if (checked) {
                Intent intent = new Intent(getApplicationContext(), playground.class);
                /*//Fixing unknown-bits-set-in-runtime-flags-0x8000
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK
                */
                //
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.Difficulty), radiotext);
                ///intent.putExtra("game",game=new Game(Difficulties.valueOf(diffSelect.toString())));
                intent.putExtras(bundle);
                startActivity(intent);
            }else{
                Toast.makeText(getApplicationContext(),"Select a difficulty please",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
