package com.example.shifuminco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.marlou.shifuminco.Difficulties;
import com.marlou.shifuminco.Game;
import com.marlou.shifuminco.Types;

public class game extends AppCompatActivity {

    //game=(Game)getIntent().getSerializableExtra("game");

    Game currentGame;
    TextView diffText;
    ImageView spock;
    ImageView scissors;
    ImageView rock;
    ImageView paper;
    ImageView lizard;
    TextView AIPlayed;
    TextView yourScore;
    TextView AIScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        //
        currentGame=new Game(getIntent().getStringExtra(getString(R.string.Difficulty)));
        diffText=findViewById(R.id.txtDifficulty);
        diffText.setText(String.format(getString(R.string.YourDifficulty), getIntent().getStringExtra(getString(R.string.Difficulty))));
        // Declaration of the Views
        spock=findViewById(R.id.imgVSpoke);
        scissors=findViewById(R.id.imgVScissors);
        rock=findViewById(R.id.imgVRock);
        paper=findViewById(R.id.imgVPaper);
        lizard=findViewById(R.id.imgVLizard);
        AIPlayed=findViewById(R.id.txtAIPlayed);
        yourScore=findViewById(R.id.txtYourScore);
        AIScore=findViewById(R.id.txtAIScore);
        // Buttons consequences
        spock.setOnClickListener((v)-> takeTurn(Types.Spock));
        scissors.setOnClickListener((v)-> takeTurn(Types.Scissors));
        rock.setOnClickListener((v)-> takeTurn(Types.Stone));
        paper.setOnClickListener((v)-> takeTurn(Types.Paper));
        lizard.setOnClickListener((v)-> takeTurn(Types.Lizard));
    }

    private void takeTurn(Types typ){
        String AIP;
        AIPlayed.setText(String.format(getString(R.string.AIPlayed), typ.name(), AIP=currentGame.Play(typ).name(), currentGame.getLastRoundState()));
        yourScore.setText(String.format(getString(R.string.yourScore), currentGame.numberOfWins()));
        AIScore.setText(String.format(getString(R.string.AIScore), currentGame.numberOfLooses()));
        if(currentGame.getIsGameFinished()){
            Intent intent=new Intent(getApplicationContext(),result.class);
            intent.putExtra(getString(R.string.PPlayed),typ.name());
            intent.putExtra(getString(R.string.AIP),AIP);
            intent.putExtra(getString(R.string.CurrentGame),currentGame);
            startActivity(intent);

        }
    }
}
