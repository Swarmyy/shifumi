package com.example.shifuminco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.icu.text.Edits;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Ranks extends AppCompatActivity {

    Button back_btn;
    DatabaseReference reference;
    ListView ranks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranks);

        back_btn = findViewById(R.id.retour_btn);
        ranks = findViewById(R.id.ranking_list);
        reference = FirebaseDatabase.getInstance().getReference().child("User");

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Menu.class));
            }
        });

        reference.orderByChild("nb_points").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            List<String> usersuid = new ArrayList<String>();

                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                    String key=childSnapshot.getKey();
                    usersuid.add(key);
                }
                List<String> usersinfo = new ArrayList<String>();
                Integer compteur = usersuid.size();
                for (String uid:usersuid) {
                    usersinfo.add("                 N°" + compteur + " - "+ dataSnapshot.child(uid).child("pseudo").getValue().toString() + " - Score: " + dataSnapshot.child(uid).child("nb_points").getValue().toString());
                    compteur -= 1;
                }
                String[] user = new String[usersinfo.size()];
                Collections.reverse(usersinfo);
                usersinfo.toArray(user);

                ArrayAdapter<String> arrayAdapter
                        = new ArrayAdapter<String>(Ranks.this, android.R.layout.simple_list_item_1 , user);



                ranks.setAdapter(arrayAdapter);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
