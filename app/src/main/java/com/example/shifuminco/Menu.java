package com.example.shifuminco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class Menu extends AppCompatActivity {

    DatabaseReference reference;
    Button play_btn, profile_btn, rules_btn, logout_btn, ranks_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        final TextView greetings = findViewById(R.id.greetings);
        reference = FirebaseDatabase.getInstance().getReference().child("User");

        play_btn = findViewById(R.id.start_button);
        profile_btn = findViewById(R.id.userinfo_button);
        rules_btn = findViewById(R.id.rules_button);
        logout_btn = findViewById(R.id.logout_button);
        ranks_btn = findViewById(R.id.ranking_btn);

        profile_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Chargement du profil", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),Profile.class));
            }
        });

        play_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AIDifficulty.class));
            }
        });

        ranks_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Ranks.class));
            }
        });



        //Récupération de l'utilisateur connecté
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            reference.child(user.getUid()).child("pseudo").addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    greetings.setText("Bonjour, " + dataSnapshot.getValue().toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    greetings.setText("Un Problème est survenu");
                }
            });
        }


        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(Menu.this, "Déconnecté", Toast.LENGTH_SHORT);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        rules_btn.setOnClickListener((v)->{
            startActivity(new Intent(getApplicationContext(),rules.class));
        });
    }

}