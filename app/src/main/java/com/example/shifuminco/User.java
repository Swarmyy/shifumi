package com.example.shifuminco;

public class User {
    public String pseudo, email;
    public Integer nb_win, nb_defeat, nb_points;

    public User(){

    }

    public User(String pseudo, String email) {
        this.pseudo = pseudo;
        this.email = email;
        nb_win = 0 ;
        nb_defeat = 0;
        nb_points = 0;
    }
}
