package com.example.shifuminco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class rules extends AppCompatActivity {

    Button btnRtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);
        //Definition of the views
        btnRtn=findViewById(R.id.btnBack);
        btnRtn.setOnClickListener((v)->{
            startActivity(new Intent(getApplicationContext(),Menu.class));
        });
    }
}
