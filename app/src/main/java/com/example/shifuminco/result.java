package com.example.shifuminco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class result extends AppCompatActivity {

    TextView result;
    TextView resultPoints;
    TextView points;
    EditText descResult;
    Button replay;
    Button logout;
    DatabaseReference reference;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);


        // Get Intent
        Intent li = getIntent();
        // DATABASE IMPLICATION
        boolean win = li.getBooleanExtra(getString(R.string.isPlayerWinner), false);
        int pointsEarned = li.getIntExtra(getString(R.string.points),0);
        reference = FirebaseDatabase.getInstance().getReference().child("User");
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // Declaration of the Views
        result = findViewById(R.id.txtResult);
        resultPoints = findViewById(R.id.txtPtResult);
        points = findViewById(R.id.txtPoints);
        descResult = findViewById(R.id.descResult);
        replay = findViewById(R.id.btnReplay);
        logout = findViewById(R.id.btnLogout);
        // setTexts
        descResult.setText(String.format(getString(R.string.descResult), li.getStringExtra(getString(R.string.PPlayed)), li.getStringExtra(getString(R.string.AIP)), li.getStringExtra(getString(R.string.lastRoundState))));
        result.setText(li.getBooleanExtra(getString(R.string.isPlayerWinner), false) ? "YOU WIN" : "YOU LOOSE");
        resultPoints.setText(String.format(getString(R.string.scoreFinal), li.getIntExtra(getString(R.string.numberOfWins), -1), li.getIntExtra(getString(R.string.numberOfLoose), -1)));
        points.setText(String.format(li.getBooleanExtra(getString(R.string.isPlayerWinner), false) ? getString(R.string.PointsEarned) : getString(R.string.PointsLost), li.getIntExtra(getString(R.string.points), -1)));
        // OnClick declaration
        replay.setOnClickListener((v) -> {
            Intent intent = new Intent(getApplicationContext(), AIDifficulty.class);
            startActivity(intent);
        });
        logout.setOnClickListener((v) -> {
            Intent intent = new Intent(getApplicationContext(), Menu.class);
            startActivity(intent);
        });

        if (win) {
            reference.child(user.getUid()).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    Integer nb_win = Integer.parseInt(dataSnapshot.child("nb_win").getValue().toString());
                    nb_win += 1;
                    reference.child(user.getUid()).child("nb_win").setValue(nb_win);
                    Integer nb_points = Integer.parseInt(dataSnapshot.child("nb_points").getValue().toString());
                    nb_points += pointsEarned;
                    reference.child(user.getUid()).child("nb_points").setValue(nb_points);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), "Un problème est survenu", Toast.LENGTH_SHORT);
                }
            });
        }else{
            reference.child(user.getUid()).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    Integer nb_defeat = Integer.parseInt(dataSnapshot.child("nb_defeat").getValue().toString());
                    nb_defeat += 1;
                    reference.child(user.getUid()).child("nb_defeat").setValue(nb_defeat);
                    Integer nb_points = Integer.parseInt(dataSnapshot.child("nb_points").getValue().toString());
                    if(nb_points>=pointsEarned){
                        nb_points -= pointsEarned;
                        reference.child(user.getUid()).child("nb_points").setValue(nb_points);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), "Un problème est survenu", Toast.LENGTH_SHORT);
                }
            });
        }
    }
}
