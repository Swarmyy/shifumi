package com.example.shifuminco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class Inscription extends AppCompatActivity {


    EditText mPseudoField,mEmailField,mPasswordField,mPasswordConfirmationField;
    Button btn_register;
    FirebaseAuth firebaseAuth;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        //Association
        mPseudoField = findViewById(R.id.pseudo_edit);
        mEmailField = findViewById(R.id.mail_edit);
        mPasswordField = findViewById(R.id.pass_edit);
        mPasswordConfirmationField = findViewById(R.id.pass_conf_edit);

        // Buttons
        btn_register = findViewById(R.id.sign_up_button);

        //Firebase
        firebaseAuth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("User");

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String pseudo = mPseudoField.getText().toString();
                final String mail = mEmailField.getText().toString();
                String pass = mPasswordField.getText().toString();

                if (!validateForm() || !mPasswordField.getText().toString().equals(mPasswordConfirmationField.getText().toString())) {
                    Toast.makeText(Inscription.this, "Une erreur est survenue, veuillez vérifier les informations saisies", Toast.LENGTH_SHORT).show();
                    return;
                }

                firebaseAuth.createUserWithEmailAndPassword(mail, pass)
                        .addOnCompleteListener(Inscription.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    User information = new User(
                                            pseudo,mail
                                    );

                                    reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                            .setValue(information).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(Inscription.this, "Inscription réussie", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                        }
                                    });

                                } else {
                                    Toast.makeText(Inscription.this, "Une erreur est survenue lors de l'inscription", Toast.LENGTH_SHORT);
                                }


                            }
                        });
            }
        });
    }

    private boolean validateForm() {
        boolean valid = true;

        String pseudo = mPseudoField.getText().toString();
        if (TextUtils.isEmpty(pseudo)) {
            mPseudoField.setError("Veuillez entrer un Pseudo");
            valid = false;
        } else {
            mPseudoField.setError(null);
        }

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Veuillez entrer un Email");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Veuillez entrer un mot de passe");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        String pass_conf = mPasswordConfirmationField.getText().toString();
        if (TextUtils.isEmpty(pass_conf)) {
            mPasswordConfirmationField.setError("Veuillez entrer un mot de passe");
            valid = false;
        } else {
            mPasswordConfirmationField.setError(null);
        }

        return valid;
    }

}