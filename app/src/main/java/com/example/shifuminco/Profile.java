package com.example.shifuminco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Profile extends AppCompatActivity {

    DatabaseReference reference;
    TextView pseudo, mail, nb_win, nb_defeat, nb_point;
    Button back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        reference = FirebaseDatabase.getInstance().getReference().child("User");

        pseudo = findViewById(R.id.pseudo_label);
        mail = findViewById(R.id.mail_prof_label);
        nb_win = findViewById(R.id.win_label);
        nb_defeat = findViewById(R.id.defeat_label);
        nb_point = findViewById(R.id.points_label);
        back_btn = findViewById(R.id.back_button);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Menu.class));
            }
        });

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            reference.child(user.getUid()).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    pseudo.setText("Pseudo: " + dataSnapshot.child("pseudo").getValue().toString());
                    mail.setText("Adresse email: " + dataSnapshot.child("email").getValue().toString());
                    nb_win.setText("Nombre de victoire: " + dataSnapshot.child("nb_win").getValue().toString());
                    nb_defeat.setText("Nombre de défaite: " + dataSnapshot.child("nb_defeat").getValue().toString());
                    nb_point.setText("Nombre de points: " + dataSnapshot.child("nb_points").getValue().toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(),"Un problème est survenu", Toast.LENGTH_SHORT);
                }
            });
        }
    }

}
