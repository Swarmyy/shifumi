package com.marlou.shifuminco;

import java.io.Serializable;
import java.util.HashSet;

/**
 * All the difficulties the AI can have
 */
public enum Difficulties implements Serializable {
    Easy(1,0.33f),

    Normal(2,0.5f),

    Hard(3,0.66f);

    public final int points;
    public final float probability;
    //public final String name;

    Difficulties(int points,float probability){
        //this.name=name;
        this.points=points;
        this.probability=probability;
    }

}
