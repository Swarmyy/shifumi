package com.marlou.shifuminco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

/**
 *
 */
public final class Fight {
    /**
     * All the relations between types
     */
    private static HashMap<Types,Types> superiorityTable1;
    private static HashMap<Types,Types> superiorityTable2;


    static {
        superiorityTable1 = new HashMap<Types, Types>() {
            {
                put(Types.Scissors, Types.Paper);
                put(Types.Paper, Types.Stone);
                put(Types.Stone, Types.Lizard);
                put(Types.Lizard, Types.Spock);
                put(Types.Spock, Types.Scissors);
            }
        };
        superiorityTable2= new HashMap<Types, Types>() {
            {
                put(Types.Scissors, Types.Lizard);
                put(Types.Lizard, Types.Paper);
                put(Types.Paper, Types.Spock);
                put(Types.Spock, Types.Stone);
                put(Types.Stone, Types.Scissors);
            }
        };
    }

    /**
     * Say who's winner of the fight
     * @param player1 Type played by the first player
     * @param player2 Type played by the second player
     * @return 1 if player 1 wins, else 2, and 0 if there is a draw
     */
    public static int WhoWins(Types player1,Types player2){
        return (player1 == player2) ? 0 : superiorityTable1.get(player1) == player2 || superiorityTable2.get(player1) == player2 ? 1 : 2;
    }

    public static ArrayList<HashMap<Types,Types>> getSuperiorityTable(){
        ArrayList<HashMap<Types,Types>> superiorityTable=new ArrayList<>();
        superiorityTable.add(superiorityTable1);
        superiorityTable.add(superiorityTable2);
        return superiorityTable;
    }
}
