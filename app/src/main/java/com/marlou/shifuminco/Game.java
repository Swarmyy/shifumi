package com.marlou.shifuminco;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public final class Game implements Serializable {

    static private int maxRounds=5;

    private List<Integer> games=new ArrayList<>();
    private boolean isPlayerWinner;
    private boolean isGameFinished;
    private String LastRoundState;
    private Difficulties difficulty;

    private AI tAI;

    public Game(String diff){
        for (int i = 0; i < Difficulties.values().length; i++)
            if(Difficulties.values()[i].toString().equals(diff))
                GenerateAI(Difficulties.values()[i]);
    }

    private void GenerateAI(Difficulties diff){
        difficulty=diff;
        isGameFinished=false;
        tAI=new AI(diff);
    }

    /**
     * Makes the AI play against the type the player entered
      * @param player1 The type the player is playing
     * @return 1 if player 1 wins, else 2, and 0 if there is a draw
     */
    public Types Play(Types player1){
        Types result=tAI.Fight(player1);
        int win=Fight.WhoWins(player1,result);
        LastRoundState= win==1?"won":win==2?"lost":"draw";
        if(win!=0){
            games.add(win);
            if(numberOfWins()>maxRounds/2||numberOfLooses()>maxRounds/2){
                isGameFinished=true;
                isPlayerWinner= numberOfWins() > maxRounds / 2;
            }
        }
        return result;
    }

    public int numberOfWins(){
        return numberOf(1);
    }

    public int numberOfLooses(){
        return numberOf(2);
    }

    private int numberOf(int state){
        int n=0;
        for (int i = 0; i < games.size(); i++)
            if (games.get(i) == state) n++;
        return n;
    }

    public boolean getIsPlayerWinner(){
        return isPlayerWinner;
    }

    public boolean getIsGameFinished(){
        return isGameFinished;
    }

    public String getLastRoundState(){ return LastRoundState; }

    public Difficulties getDifficulty() { return difficulty; }

    public static int getMaxRounds(){
        return maxRounds;
    }

}
