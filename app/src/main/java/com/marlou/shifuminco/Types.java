package com.marlou.shifuminco;

/**
 * All the types available for you to play
 */
public enum Types {
    Stone,
    Paper,
    Scissors,
    Lizard,
    Spock
}
