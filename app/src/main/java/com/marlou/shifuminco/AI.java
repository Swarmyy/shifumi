package com.marlou.shifuminco;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

/**
 *
 */
public class AI {

    private Difficulties diff;

    /**
     *
     * @param diff The difficulty of the AI
     */
    public AI(Difficulties diff){
        this.diff=diff;
    }

    /**
     * Makes the IA fight against the playerValue inputted, result value is determined randomly so be careful
     * @param playerValue The Type the player has played
     * @return The type the AI plays
     */
    public Types Fight(Types playerValue){
        return ChanceToWin(playerValue,diff.probability);
    }

    /**
     * Determine a chance of winning
     * @param playerValue
     * @param chance The chance you have of winning
     * @return
     */
    private Types ChanceToWin(Types playerValue,float chance){
        double drew=Math.random();
        return drew<chance?Win(playerValue):drew<0.9?Lose(playerValue):playerValue;
    }

    private Types Lose(Types playerValue){
        Random random=new Random();
        return Fight.getSuperiorityTable().get(random.nextInt(2)).get(playerValue);
    }

    private Types Win(Types playerValue){
        Random random=new Random();
        List<Types> losingWays= getKeysByValue(Fight.getSuperiorityTable().get(random.nextInt(2)),playerValue);
        return losingWays.get(random.nextInt(losingWays.size()));
    }

    /**
     * https://stackoverflow.com/questions/13692700/good-way-to-get-any-value-from-a-java-set
     * @param map
     * @param value
     * @param <T>
     * @param <E>
     * @return
     */
    public static <T, E> List<T> getKeysByValue(Map<T, E> map, E value) {
        List<T> keys = new ArrayList<>();
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                keys.add(entry.getKey());
            }
        }
        return keys;
    }
}
